import { NewCard } from "./NewCard";
import news from "./news.json";
import styles from "./NewsGrid.module.css"

export function NewsGrid() {
    return (
        <ul className={styles.newsGrid}>
            {news.data.map((news) => (
                <NewCard key={news.id} news={news} />
            ))}
        </ul>
    );
}
