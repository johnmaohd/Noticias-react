import styles from "./NewCard.module.css"
export function NewCard({ news }) {
    const imageUrl = "http://18.117.88.83/gocatastral/web/sites/default/files/styles/card1/public/prensa/go1.jpeg?itok=YPt0GgEN"
    return <li className={styles.newCard}>
        <img
            width={300}
            className={styles.newImage}
            src={imageUrl}
            alt={news.relationships.field_image.data.meta.alt} />
        <div>{news.attributes.title}</div>
        <div>{news.attributes.created}</div>
        <div>{news.relationships.field_image.links.related.href}</div>
        <a href={news.relationships.field_image.links.related.href}>{news.relationships.field_image.data.meta.alt}</a>
        </li>
}

