import {NewsGrid} from "./NewsGrid";
import styles from "./App.module.css"

export function App() {
    return <div>
        <header>
            <div className={styles.clogo} >
                <img className={styles.logo} src="https://cdn.www.gov.co/assets/images/logo.svg"/>
            </div>

            <div>
                <img className={styles.c2logo} src="https://www.sic.gov.co/sites/default/files/images/LogoSIC-8.png"/>
            </div>
            <nav>
                <ul className={styles.menu}>
                    <li className={styles.menuItem}><a href="#">Inicio</a></li>
                    <li className={styles.menuItem}><a href="#">Transparencia y acceso a la información pública</a></li>
                    <li className={styles.menuItem}><a href="#">Noticias</a></li>
                    <li className={styles.menuItem}><a href="#">Servicios</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <NewsGrid/>
        </main>
    </div>;
}




